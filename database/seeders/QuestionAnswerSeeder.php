<?php

namespace Database\Seeders;

use App\Models\QuestionAnswer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class QuestionAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = array(
            [
                'question' => [
                    'uz' => 'Sayt yaratish uchun qancha vaqt ketadi?',
                    'ru' => 'Сколько времени занимает создание сайта?',
                    'en' => 'How long does it take to create a site?',
                ],
                'answer' => [
                    'uz' => "Bu, ehtimol, eng ko'p beriladigan savollardan biridir.Saytni ishlab chiqishga sarflangan vaqt miqdori bir necha omillarga bog'liq: sayt turi, dizaynning murakkabligi, saytning ishlashi, sayt tuzilishi, sahifalar soni.
                    Vizitka saytini ishlab chiqish uchun o'rtacha 2 dan 5 kungacha, Landing Page-1 dan 10 kungacha, onlayn - do'kon --- 5 dan 20 kungacha, portfoliyo sayti yoki shaxsiy blog — 5 kundan.
                    
                    Albatta, bu raqamlar taxminiydir. Saytingizni ishlab chiqish qancha davom etishini bilish uchun onlayn ariza qoldiring, biz sizga qo'ng'iroq qilamiz va saytingizni yaratish narxi va shartlari bo'yicha aniq hisob-kitoblarni jo'natamiz.
                    ",
                    'ru' => 'Это, наверное, один из самых часто задаваемых вопросов. Количество времени потраченного на разработку сайта зависит от нескольких факторов: вид сайта, сложность дизайна, функционал сайта, структура сайта, количество страниц. В среднем на разработку сайта-визитки уходит от 2 до 5 дней, Landing Page — от 1 до 10 дней, Интернет-магазин — от 5 до 20 дней, сайт портфолио или личный блог — от 5 дней.

                    Конечно же, эти цифры приблизительные. Для того, чтобы узнать сколько времени займет разработка Вашего сайта, оставьте онлайн-заявку, мы перезвоним Вам и отправим точные расчеты стоимости и сроков создания Вашего сайта.',
                    'en' => "This is probably one of the most frequently asked questions. The amount of time spent on developing a site depends on several factors: the type of site, the complexity of the design, the performance of the site, the structure of the site, the number of pages.
                    On average, it takes 2 to 5 days to develop a business card site, Landing Page-1 to 10 days, an online store --- from 5 to 20 days, a portfolio site or a personal blog — from 5 days.
                    
                    Of course, these numbers are approximate. To find out how long it will take to develop your site, submit an online application, we will call you and send you an accurate estimate of the cost and terms of creating your site.",
                ],
            ],
            [
                'question' => [
                    'uz' => 'Saytimning rivojlanish jarayonini kuzata olamanmi?',
                    'ru' => 'Могу ли я следить за процессом разработки моего сайта?',
                    'en' => 'Can I track the progress of my site?',
                ],
                'answer' => [
                    'uz' => "Albatta! Saytni rivojlantirishning har bir bosqichini ko'rishingiz mumkin.
                    Ya’ni, xosting sotib olish va domenni ro'yxatdan o'tkazishdan veb-saytni yakuniy optimallashtirishgacha. Biz saytni rivojlantirish va targ'ib qilishning har bir bosqichi yakunida bajarilgan ishlar bo'yicha hisobotlarni taqdim etamiz. Saytning har bir detali mijoz bilan kelishiladi.",
                    'ru' => 'Конечно! Вы можете видеть каждый этап разработки сайта. От покупки хостинга и регистрации домена до финальной оптимизации сайта. Мы предоставляем отчеты о выполненных работах при завершении каждого этапа разработки и продвижения сайта. Каждая деталь сайта согласовываться с заказчиком.',
                    'en' => "Of course! You can see every stage of site development.
                    That is, from the purchase of hosting and domain registration to the final optimization of the website. We provide progress reports at the end of each stage of site development and promotion. Every detail of the site is agreed with the client.",
                ],
            ],
            [
                'question' => [
                    'uz' => "Qo'shimcha to'lovlarsiz veb-saytga qanday o'zgartirishlar kiritishim mumkin?",
                    'ru' => 'Какие изменения я могу внести в веб-сайт без дополнительной платы?',
                    'en' => "How can I make changes to the website without additional fees?",
                ],
                'answer' => [
                    'uz' => "Biz sizga veb-sayt administrator panelini taqdim etamiz, u erda siz tarkibni, sayt tuzilishini tahrirlashingiz, saytga tashriflar bo'yicha tahlillarni, trafik manbalarini, etakchi tahlillarni ko'rishingiz mumkin. Buning uchun sizga sayt admin panelida ishlash bo'yicha video darsliklarni yuboramiz. Qo'shimcha savollarizga javoblarni Veb-dasturchimiz bilan maslahatlashib olishingiz mumkun.",
                    'ru' => 'Мы предоставляем админку веб-сайта Вам, там Вы сможете редактировать контент, структуру сайта, видеть аналитику по посещениям сайта, источникам трафика, аналитику по лидам. Для этого отправим Вам видеоуроки про работу в админке сайта. По дополнительным вопросам Вас будет консультировать наш Веб-разработчик.',
                    'en' => "We provide you with a website admin panel, where you can edit content, site structure, view analytics on site visits, traffic sources, lead analytics. For this, we will send you video tutorials on working in the site admin panel. You can get answers to your additional questions by consulting with our web developer.",
                ],
            ],
            [
                'question' => [
                    'uz' => "Veb-saytni ishlab chiqish narxiga yana qanday xizmatlar kiradi?",
                    'ru' => 'Какие еще услуги включены в стоимость разработки сайта?',
                    'en' => "What other services are included in the price of website development?",
                ],
                'answer' => [
                    'uz' => "Veb-saytni ishlab chiqish narxiga yana qanday xizmatlar kiradi?",
                    'ru' => 'В стоимость создания сайта включены — настройка хостинга, регистрация доменного имени, создание E-mail аккаунта.',
                    'en' => "The price of creating a site includes - setting up hosting, registering a domain name, creating an account in e-mail.",
                ],
            ],
            [
                'question' => [
                    'uz' => "Mening saytim mobil telefonlarda to'g'ri ko'rsatiladimi?",
                    'ru' => 'Будет ли мой сайт правильно отображаться на мобильных телефонах?',
                    'en' => "Will my site display correctly on mobile phones?",
                ],
                'answer' => [
                    'uz' => "Bizning barcha veb-saytlarimiz moslashuvchan dizaynga ega va har qanday qurilmada to'g'ri ko'rsatiladi. Sayt katta monitorlar, noutbuklar, planshetlar va telefonlarda to'g'ri ko'rsatiladi.
                    ",
                    'ru' => 'Все наши веб-сайты имеют адаптивным дизайн и корректно отображаются на любых устройствах. Сайт будет отображаться правильно на больших мониторах, ноутбуках, планшетах и телефонах.',
                    'en' => "All our websites have a responsive design and display correctly on any device. The site displays correctly on large monitors, laptops, tablets and phones.",
                ],
            ],
        );
        foreach($data as $d) {
            QuestionAnswer::query()->create([
                'question' => $d['question'],
                'answer' => $d['answer'],
            ]);
        }
        
    }
}