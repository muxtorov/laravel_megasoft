<?php

namespace Database\Seeders;

use App\Models\Detail;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DetailSeeder extends Seeder
{
    public function run(): void
    {
        $data = [
            [
                'title' => [
                    'uz' => "Onlayn do'kon",
                    'ru' => 'Интернет-магазин',
                    'en' => 'Online store',
                ],
                'description' => [
                    'uz' => "Onlayn do'kon foydalanuvchilarga Internet orqali tovarlar va xizmatlarni ko'rib chiqish va sotib olish imkonini beruvchi elektron tijorat resursidir. Onlayn do'konda mahsulotlar to'g'ri mahsulotni topishni osonlashtiradigan toifalarga ajratilishi mumkin. Onlayn do'kon egalari kredit kartalari, PayPal va boshqa onlayn to'lov tizimlarini o'z ichiga olgan turli to'lov usullaridan foydalanishlari mumkin.",
                    'ru' => 'Интернет-магазин - это электронный коммерческий ресурс, который позволяет пользователям просматривать и покупать товары и услуги через Интернет. В интернет-магазине товары могут быть организованы в категории, которые облегчают поиск нужного продукта. Владельцы интернет-магазина могут использовать различные методы оплаты, включая кредитные карты, PayPal и другие онлайн-платежные системы.',
                    'en' => 'An online store is an electronic commerce resource that allows users to browse and buy goods and services over the Internet. In an online store, products can be organized into categories that make it easier to find the right product. Online store owners can use a variety of payment methods, including credit cards, PayPal, and other online payment systems.',
                ],
                'image' => 'internet-magazine.jpeg'
            ],
            [
                'title' => [
                    'uz' => "To'lov tizimlari, logistika va ombor hisobi bilan integratsiya",
                    'ru' => 'Интеграция с платежными системами, логистикой и складским учетом',
                    'en' => 'Integration with payment systems, logistics and warehouse accounting',
                ],
                'description' => [
                    'uz' => "Toʻlov tizimlari, logistika va ombor hisobi bilan integratsiya onlayn-doʻkonni yaratish va boshqarish jarayonining muhim qismidir. To'lov tizimlari bilan integratsiya mijozlardan onlayn to'lovlarni qabul qilish imkonini beradi, bu onlayn-do'konning ishlashi uchun zaruriy shartdir. Buning uchun siz mos to'lov tizimini tanlashingiz va uni do'kon veb-saytida sozlashingiz kerak. Eng keng tarqalgan to'lov tizimlaridan ba'zilari Payme, Click, Uzum va boshqalarni o'z ichiga oladi.",
                    'ru' => 'Интеграция с платежными системами, логистикой и учетом склада является важной частью процесса создания и управления интернет-магазином. Интеграция с платежными системами позволяет принимать онлайн-платежи от покупателей, что является необходимым условием для работы интернет-магазина. Для этого необходимо выбрать подходящую платежную систему и настроить ее на сайте магазина. Некоторые из наиболее распространенных платежных систем включают Payme, Click, Uzum и др.',
                    'en' => "Integration with payment systems, logistics and warehouse accounting is an important part of the process of creating and managing an online store. Integration with payment systems allows you to accept online payments from customers, which is a prerequisite for the operation of an online store. To do this, you need to choose a suitable payment system and set it up on the store's website. Some of the most common payment systems include Payme, Click, Uzum and more.",
                ],
                'image' => 'payment-system.png'
            ],
            /** Корпоративный веб-сайт */
            [
                'title' => [
                    'uz' => "Korporativ veb-sayt",
                    'ru' => 'Корпоративный веб-сайт',
                    'en' => 'Corporate website',
                ],
                'description' => [
                    'uz' => "Korporativ veb-sayt - bu kompaniyani, faoliyatini, mahsulotlarini va xizmatlarini taqdim etish uchun mo'ljallangan sayt. U kompaniyani eng yaxshi ko'rinishda taqdim etish, yangi mijozlarni jalb qilish va mavjud mijozlar bilan aloqalarni kuchaytirish imkonini beradi. Korporativ veb-sayt kompaniyaning imidjini aks ettirish uchun professional ravishda ishlab chiqilishi va o'zgartirilishi kerak. U oson o'qilishi va navigatsiyasi, qulay tuzilishi va kompaniya va uning mahsulotlari yoki xizmatlari haqida aniq ma'lumotlarni o'z ichiga olishi kerak.",
                    'ru' => "Корпоративный веб-сайт - это сайт, который предназначен для представления компании, ее деятельности, продуктов и услуг. Он позволяет представить компанию в наилучшем свете, привлечь новых клиентов и укрепить связи с уже существующими. Корпоративный веб-сайт должен быть профессионально разработан и оформлен, чтобы отражать имидж компании. Он должен быть легко читаемым и навигируемым, иметь удобную структуру и содержать четкую информацию о компании и ее продуктах или услугах.",
                    'en' => "A corporate website is a website designed to present a company, its activities, products and services. It allows you to present the company in the best light, attract new customers and strengthen ties with existing ones. The corporate website must be professionally designed and decorated to reflect the company's image. It should be easy to read and navigate, have a convenient structure and contain clear information about the company and its products or services.",
                ],
                'image' => 'corporate-website.jpeg'
            ],
            [
                'title' => [
                    'uz' => "Sizning kompaniyangizning Internetdagi eng samarali vakili",
                    'ru' => 'Максимально эффективное представительство Вашей компании в Интернете',
                    'en' => 'The most effective representation of your company on the Internet',
                ],
                'description' => [
                    'uz' => "Kompaniyaning Internetdagi eng samarali vakili har tomonlama va maqsadli yondashuvni talab qiladi. Quyida kompaniyangizning Internetdagi mavjudligi samaradorligini oshirishga yordam beradigan asosiy fikrlar keltirilgan: Professional veb-sayt yaratish: Kompaniyaning veb-sayti professional tarzda ishlab chiqilgan bo'lishi va kompaniya imidjini aks ettirishi, o'qish va harakatlanish uchun qulay bo'lishi, foydalanuvchilarga qulay tuzilishga ega bo'lishi va kompaniya va uning mahsuloti yoki xizmatlari haqida aniq ma'lumotlarni o'z ichiga olishi kerak.Veb-saytni optimallashtirish: Qidiruv mexanizmini optimallashtirish (SEO) qidiruv tizimining reytingini yaxshilashga yordam beradi va shuning uchun potentsial mijozlarga ko'rinishini oshiradi.",
                    'ru' => "Максимально эффективное представительство компании в Интернете требует комплексного и целенаправленного подхода. Ниже приведены некоторые ключевые моменты, которые могут помочь повысить эффективность представительства вашей компании в Интернете: Создание профессионального веб-сайта: веб-сайт компании должен быть профессионально разработан и оформлен, чтобы отражать имидж компании, быть легко читаемым и навигируемым, иметь удобную структуру и содержать четкую информацию о компании и ее продуктах или услугах.Оптимизация сайта: оптимизация сайта для поисковых систем (SEO) поможет улучшить его рейтинг в поисковых системах и, следовательно, увеличить его видимость для потенциальных клиентов.",
                    'en' => "The most effective representation of the company on the Internet requires a comprehensive and targeted approach. Below are some key points that can help increase the effectiveness of your company's representation on the Internet: Creating a professional website: the company's website must be professionally designed and decorated to reflect the company's image, be easy to read and navigate, have a convenient structure and contain clear information about the company and its products or services. Website optimization: website optimization for search engines (SEO) will help improve its ranking in search engines and, therefore, increase its visibility to potential customers.",
                ],
                'image' => 'vakil1.jpeg'
            ],
            [
                'title' => [
                    'uz' => "Mobil ilovalar",
                    'ru' => 'Мобильные приложения',
                    'en' => 'Mobile applications',
                ],
                'description' => [
                    'uz' => "Mobil ilovalar - bu smartfon va planshetlar kabi mobil qurilmalarda foydalanish uchun mo'ljallangan dasturiy ilovalar. Ularni App Store yoki Google Play kabi ilovalar do‘konlaridan yuklab olish va mobil qurilmaga o‘rnatish mumkin. Mobil ilovalar foydalanuvchilar va kompaniyalar uchun ko'plab afzalliklarni beradi:Foydalanish qulayligi: Mobil ilovalardan foydalanish oson va foydalanuvchilarga maʼlumotlar, mahsulotlar va xizmatlardan tezda foydalanish imkonini beradi. Mijozlarning sodiqligini oshirish: Mobil ilovalar kompaniya va uning mijozlari oʻrtasidagi aloqani mustahkamlashga yordam beradi, ularga tovarlarga buyurtma berish yoki buyurtma berishning qulay usulini taqdim etadi. xizmatlar, shuningdek, shaxsiy ma'lumotlar va tavsiyalar taqdim etish",
                    'ru' => "Мобильные приложения - это программные приложения, предназначенные для использования на мобильных устройствах, таких как смартфоны и планшеты. Их можно загрузить из магазинов приложений, таких как App Store или Google Play, и установить на мобильное устройство. Мобильные приложения предоставляют множество преимуществ как для пользователей, так и для компаний: Удобство использования: мобильные приложения обеспечивают легкий доступ к информации, продуктам и услугам для пользователей. Улучшение лояльности клиентов: мобильные приложения помогают укрепить связь между компанией и ее клиентами, предоставляя им удобный способ заказа товаров или услуг, а также предоставляя персональную информацию и рекомендации.",
                    'en' => "Mobile applications are software applications designed for use on mobile devices such as smartphones and tablets. They can be downloaded from app stores such as the App Store or Google Play and installed on a mobile device. Mobile applications provide many benefits for both users and companies: Ease of use: mobile applications provide easy access to information, products and services for users. Improving customer loyalty: mobile applications help strengthen the relationship between the company and its customers by providing them with a convenient way to order goods or services, as well as providing personal information and recommendations."
                ],
                'image' => 'mobile.jpeg'
            ],
            [
                'title' => [
                    'uz' => 'Webview / PWA / Native ilovalar iOS & Android uchun. Ideadan publikatsiya  qilishgacha',
                    'ru' => 'Webview / ПWА / Нативе приложения для иОС & Андроид. От идеи до публикации',
                    'en' => 'Webview / PWA / Native applications for iOS & Android. From idea to publication',
                ],
                'description' => [
                    'uz' => "Webview, PWA yoki native mobil ilova yaratishning asosiy bosqichi quyidagilardan iborat: Ixtiyoriy - ilova uchun konsept va asosiy xususiyatlarni o'ylab chiqing. Foydalanuvchi oqimi va foydalanuvchi tajribasini aniqlang. Dizayn - Figma, Adobe XD va h.k kabi vositalardan foydalanib UI mockup va dizaynlarni yarating. Bu ilovani rivojlantirishdan oldin tasvirlashga yordam beradi. Frontend Development - HTML, CSS va JavaScriptdan foydalanib ilovaning frontend qismini yarating. Webview ilovasi uchun bu mobil qurilmalarda yaxshi ishlaydigan moslashuvchan veb ilovasi bo'ladi",
                    'ru' => "Вот основные шаги по созданию веб-представления, PWA или нативного мобильного приложения: Идея - Придумайте концепцию и основные функции приложения. Определите поток пользователя и пользовательский опыт. Дизайн - Создайте макеты пользовательского интерфейса и дизайны с помощью инструментов, таких как Figma, Adobe XD и т. Д. Это помогает визуализировать приложение перед разработкой. Frontend Development - Создайте фронтенд приложения с помощью HTML, CSS и JavaScript. Для веб-представления это будет отзывчивое веб-приложение, которое хорошо работает на мобильных устройствах",
                    'en' => 'Here are the major steps to build a webview, PWA, or native mobile app: Idea - Come up with the concept and core features for the app. Define the user flow and user experience. Design - Create UI mockups and designs using tools like Figma, Adobe XD, etc. This helps visualize the app before developing. Frontend Development - Build the frontend of the app using HTML, CSS and JavaScript.For a webview app, this will be a responsive web app that works well on mobile devices',
                    ],
                'image' => 'pwa1.png'
            ],
            [
                'title' => [
                    'uz' => "Telegram bot",
                    'ru' => 'Телеграм-бот',
                    'en' => 'Telegram bot',
                ],
                'description' => [
                    'uz' => "Telegram botlari Telegram messenjeri ichida ishlaydigan va turli vazifalarni bajara oladigan dasturlardir. Ular xabarlarni qayta ishlashlari, xabarlar jo'natishlari, ma'lumotlarni saqlashlari va hokazo. Botlar foydalanuvchilar tomonidan yaratiladi va boshqariladi, ular tayyor botlardan foydalanishi yoki o'zlarini yaratishi mumkin.Telegram botini yaratish Telegramda bot ishlab chiqaruvchisi sifatida ro'yxatdan o'tish va API kalitini olishdan boshlanadi. Bu kalit botga Telegram serverlari bilan bog‘lanish va vazifalarni bajarish imkoniyatini beradi. Keyin bot kodi yaratiladi, uni Python, JavaScript, PHP va boshqalar kabi turli dasturlash tillarida yozish mumkin",
                    'ru' => "Телеграм-боты - это программы, которые работают внутри Telegram Messenger и могут выполнять различные задачи. Они могут перерабатывать сообщения, отправлять сообщения, сохранять данные и т. Д. Боты создаются и управляются пользователями, они могут использовать готовые боты или создавать свои собственные. Создание телеграмм-бота начинается с регистрации в качестве разработчика ботов в Telegram и получения API-ключа. Этот ключ дает боту возможность связаться с серверами Telegram и выполнить задачи. Затем создается код бота, который может быть написан на различных языках программирования, таких как Python, JavaScript, PHP и т. Д.",
                    'en' => "Telegram bots are programs that work inside Telegram Messenger and can perform various tasks. They can process messages, send messages, store data, etc. Bots are created and managed by users, they can use ready-made bots or create their own. Creating a Telegram bot starts with registering as a bot developer on Telegram and getting an API key. This key gives the bot the ability to connect to Telegram servers and perform tasks. Then the bot code is created, which can be written in various programming languages such as Python, JavaScript, PHP, etc.",
                ],
                'image' => 'telegram-bot.png'
            ],
            [
                'title' => [
                    'ru' => 'Обычный автоответчик или WebApp с мощным функционалом для бизнеса',
                    'uz' => "Oddiy avtomatik javobgar yoki biznes uchun kuchli funktsionalga ega bo'lgan WebApp",
                    'en' => 'A simple autoresponder or a WebApp with powerful functionality for business',
                ],
                'description' => [
                    'ru' => 'На мой взгляд лучше выбрать WebApp с повышенным функционалом: Преимущества веб-приложения: Высокая доступность. Веб-приложение доступно из любой точки мира на любом устройстве. Простота развертывания и обновления. Не требуется публикация в App Store и Google Play, достаточно перезапустить веб-сервер. Масштабирование. Веб-приложение может легко масштабироваться под увеличивающуюся нагрузку. Функциональность. Веб-приложения могут иметь более богатый функционал, включая регистрацию, аутентификацию, хранение данных, админ-панель и т.д',
                    'uz' => "Mening fikrimcha, kuchli funktsionalga ega bo'lgan WebAppni tanlash afzalroq: Web-App afzalliklari: Yuqori darajada mavjudlik. Web-App dunyoning istalgan nuqtasidan istalgan qurilmada mavjud. O'rnatish va yangilash osonligi. App Store va Google Playda nashr qilish talab qilinmaydi, balki veb-serverni qayta ishga tushirish yetarli. Masshtablash. Web-App o'sayotgan yukni osonlik bilan masshtablash mumkin. Funktsional. Web-App lar ro'yxatdan o'tish, autentifikatsiya, ma'lumotlarni saqlash, admin panel va hokazo kabi ko'proq funktsiyaga ega bo'lishi mumkin",
                    'en' => "In my opinion, it is better to choose a WebApp with increased functionality: Web-App advantages: High availability. The Web-App is available from anywhere in the world on any device. Ease of deployment and updating. No need to publish in the App Store and Google Play, just restart the web server. Scaling. The Web-App can easily scale to increasing load. Functionality. Web-Apps can have richer functionality, including registration, authentication, data storage, admin panel, etc.",
                ],
                'image' => 'apk.jpg'
            ]

        ];

        foreach ($data as $service) {
            Detail::create([
                'title' => $service['title'],
                'description' => $service['description'],
                'image' => $service['image'],
            ]);
        }
    }
}
