<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('corusels', function (Blueprint $table) {
            $table->id();
            $table->jsonb("name")->required();
            $table->jsonb("profession")->required();
            $table->string("image")->required();
            $table->jsonb("description")->required();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('corusels');
    }
};
