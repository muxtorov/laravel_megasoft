<?php

return [
    'home' => 'Bosh sahifa',
    'about' => 'Biz haqimizda',
    'features' => 'Xizmatlar',
    'get-started' => 'Boshlash',
    'useful-links' => 'Foydali havolalar',
    'contact' => 'Aloqa',
    'portfolio'=>'Portfolio',
    'location' => 'Manzil',
    'usefull' => 'Foydali havolalar',
    'get_started' => 'Boshlash',
    'send' => 'Yuborish',
    'your_name' => 'Ismingiz',
    'your_email' => 'E-mail',
    'tel' => 'Telefon raqam',
    'message' => 'Xabar',
];
