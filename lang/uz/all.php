<?php

return [
    'your_message_sended_succussfull' => 'Sizning xabaringiz yuborildi. Siz bilan tez orada bog\'lanamiz!',
    'title' => 'Sarlavha',
    'description' => 'Tavsif',
    'portfoliolar' => 'Portfoliolar',
    'ourportfolio' => 'Bizning Portfolio',
    'url' => 'mamzil',
    'question' => 'Savol',
    'answer' => 'Javob',
    'name' => 'Ism',
    'profession' => 'Kasbi'
];
