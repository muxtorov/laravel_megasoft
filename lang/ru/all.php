<?php

return [
    'your_message_sended_succussfull' => 'Ваше сообщение было отправлено. Мы свяжемся с вами в ближайшее время!',
    'title' => 'Заголовок',
    'description' => 'Описание',
    'portfoliolar' => 'Портфель',
    'ourportfolio' =>'Наши Портфолио',
    'url' => 'URL',
    'question' => 'Вопрос',
    'answer' => 'Отвечать',
    'name' => 'Имя',
    'profession' => 'Профессия'
];
