<?php

return [
    'home' => 'Главная',
    'about' => ' О нас',
    'features' => 'Услуги',
    'get-started' => 'Начать',
    'contact' => 'Контакт',
    'portfolio' => 'Портфолио',
    'location' => 'Адрес',
    'usefull' => 'Полезные ссылки',
    'get_started' => 'Начать',
    'send' => 'Отправить',
    'your_name' => 'Ваше имя',
    'your_email' => 'Ваш E-mail',
    'tel' => 'Номер телефона',
    'message' => 'Сообщение',
];
