<?php

return [
    'home' => 'Home',
    'about' => 'About us',
    'features' => 'Services',
    'contact' => 'Contact',
    'portfolio' => 'Portfolio',
    'location' => 'Location',
    'usefull' => 'Useful Links',
    'get_started' => 'Get Started',
    'send' => 'Send',
    'your_name' => 'Your name',
    'your_email' => 'Your E-mail',
    'tel' => 'Phone number',
    'message' => 'Message',
];
