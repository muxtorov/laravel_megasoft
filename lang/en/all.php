<?php

return [
    'your_message_sended_succussfull' => 'Your message has been sent. We will contact you soon!',
    'title' => 'Title',
    'description' => 'Description',
    'portfoliolar' => 'Portfolios',
    'ourportfolio' => 'Our Portfolios',
    'url' => 'Url',
    'question' => 'Question',
    'answer' => 'Answer',
    'name' => 'Name',
    'profession' => 'Profession'
];
