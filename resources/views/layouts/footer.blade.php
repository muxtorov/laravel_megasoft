<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row d-flex justify-content-between">

          <div class="col-lg-6 col-md-6">
            <div class="footer-info">
              <h3>{{getTranslate('mega')->title[app()->getLocale()] ?? "" }}</h3>
              <p class="pb-3"><em>{{getTranslate('mega')->content[app()->getLocale()] ?? "" }}.</em></p>
              <div class="social-links mt-3">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-2 col-md-6 footer-links footer-headers">
            <h4>{{__('lang.usefull')}}</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#hero">{{__('lang.home')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#about">{{__('lang.about')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#detail">{{__('lang.features')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#gallery">{{__('lang.portfolio')}}</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#contact">{{ __('lang.contact') }}</a></li>
            </ul>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>{{__('lang.location')}}</h4>
            <ul>
              <li class="mb-3"><i class="bi bi-geo-alt"></i> <a href="#" class="ms-3">{{getTranslate('location')->content[app()->getLocale()] ?? ''}} </a></li>
              <li class="mb-3"><i class="bi bi-phone"></i> <a href="#" class="ms-3" >{{getTranslate('call')->content[app()->getLocale()] ?? ''}}</a></li>
              <li><i class="bi bi-envelope"></i> <a href="#" class="ms-3" > {{ getTranslate('email')->content[app()->getLocale()] ?? ""}} </a></li>
            </ul>
          </div>


            </div>
        </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>MegaSoft</span></strong>. All Rights Reserved
      </div>
    </div>
</footer><!-- End Footer -->
