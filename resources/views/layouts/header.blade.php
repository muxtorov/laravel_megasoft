  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
      <div class="container d-flex align-items-center justify-content-between">

          <div class="logo">
              <h1><a href="/"><span>MegaSoft</span></a></h1>
              <!-- Uncomment below if you prefer to use an image logo -->
              <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
          </div>

          <nav id="navbar" class="navbar">
              <ul>
                  <li><a class="nav-link scrollto active" href="#hero">{{ __('lang.home') }}</a></li>
                  <li><a class="nav-link scrollto" href="#about">{{ __('lang.about') }}</a></li>
                  <li><a class="nav-link scrollto" href="#details">{{ __('lang.features') }}</a></li>
                  <li><a class="nav-link scrollto" href="#gallery">{{ __('lang.portfolio') }}</a></li>

                  <li><a class="nav-link scrollto" href="#contact">{{ __('lang.contact') }}</a></li>
                  <li class="dropdown"><a href="#" class="text-uppercase"><span>
                              {{ app()->getLocale() }}
                          </span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
                      <ul>
                          @foreach (getLocales() as $locale)
                              @if (app()->getLocale() != $locale)
                                  <li><a href="#" class="text-uppercase"
                                          onclick="changeLang('{{ $locale }}')">{{ $locale }}</a></li>
                              @endif
                          @endforeach
                      </ul>
                  </li>
              </ul>
              <i class="bi bi-list mobile-nav-toggle"></i>
          </nav><!-- .navbar -->

      </div>
  </header><!-- End Header -->
  <script>
      function changeLang(lang) {
          const urls = "/lang/" + lang;
          window.location.href = urls;
      }
  </script>
