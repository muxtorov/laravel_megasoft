@extends('layouts.app')

@section('content')
    <section id="hero">

        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex align-items-center">
                    <div data-aos="zoom-out" class="aos-init aos-animate">
                        <h1>{{ getTranslate('banner')->title[app()->getLocale()] ?? '' }}</h1>
                        <h2>{{ getTranslate('banner')->content[app()->getLocale()] ?? '' }}</h2>
                        <div class="text-center text-lg-start">
                            <a href="#about" class="btn-get-started scrollto">{{ __('lang.get_started') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2 hero-img aos-init  aos-animate" data-aos="zoom-out" data-aos-delay="300">
                    <img src="{{ asset('assets/img/banner.jpg') }}" class="img-fluid animated" alt="">
                </div>
            </div>
        </div>

        <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
            viewBox="0 24 150 28 " preserveAspectRatio="none">
            <defs>
                <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
            </defs>
            <g class="wave1">
                <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
            </g>
            <g class="wave2">
                <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
            </g>
            <g class="wave3">
                <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
            </g>
        </svg>

    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= About Section ======= -->
        <section id="about" class="about">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch"
                        data-aos="fade-right">
                        {{--            <a href="https://www.youtube.com/watch?v=StpBR2W8G5A" class="glightbox play-btn mb-4"></a> --}}
                    </div>
                    <div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5"
                        data-aos="fade-left">
                        <h3>{{ getTranslate('service')->title[app()->getLocale()] ?? '' }}</h3>
                        <p>{{ getTranslate('service')->content[app()->getLocale()] ?? '' }}</p>

                        <div class="icon-box" data-aos="zoom-in" data-aos-delay="100">
                            <div class="icon"><img src="/storage/{{ getTranslate('ecommerce')->icon }}" alt="">
                            </div>
                            <h4 class="title"><a
                                    href="">{{ getTranslate('ecommerce')->title[app()->getLocale()] ?? '' }}</a>
                            </h4>
                            <p class="description">{{ getTranslate('ecommerce')->content[app()->getLocale()] ?? '' }}</p>
                        </div>

                        <div class="icon-box" data-aos="zoom-in" data-aos-delay="200">
                            <div class="icon"><img src="/storage/{{ getTranslate('corporate')->icon }}" alt="">
                            </div>
                            <h4 class="title"><a
                                    href="">{{ getTranslate('corporate')->title[app()->getLocale()] ?? '' }}</a>
                            </h4>
                            <p class="description">{{ getTranslate('corporate')->content[app()->getLocale()] ?? '' }}</p>
                        </div>

                        <div class="icon-box" data-aos="zoom-in" data-aos-delay="300">
                            <div class="icon"><img src="/storage/{{ getTranslate('mobile')->icon }}" alt="Mobile"
                                    class="img-fluid"></div>
                            <h4 class="title"><a
                                    href="">{{ getTranslate('mobile')->title[app()->getLocale()] ?? '' }}</a></h4>
                            <p class="description">{{ getTranslate('mobile')->content[app()->getLocale()] ?? '' }}</p>
                        </div>
                        <div class="icon-box" data-aos="zoom-in" data-aos-delay="300">
                            <div class="icon"><img src="/storage/{{ getTranslate('telegrambot')->icon }}" alt="">
                            </div>
                            <h4 class="title"><a
                                    href="">{{ getTranslate('telegrambot')->title[app()->getLocale()] ?? '' }}</a>
                            </h4>
                            <p class="description">{{ getTranslate('telegrambot')->content[app()->getLocale()] ?? '' }}
                            </p>
                        </div>

                    </div>
                </div>

            </div>
        </section><!-- End About Section -->



        <!-- ======= Details Section ======= -->
        <section id="details" class="details">
            <div class="container">

                @foreach ($details as $detail)
                    <?php
                    $id = $detail['id'] % 2;
                    ?>
                    <div class="row content">
                        @if ($id == 1)
                            <div class="col-md-4" data-aos="fade-right">
                                <img src="/storage/<?= $detail['image'] ?>" class="img-fluid" alt="">
                            </div>
                        @elseif($id == 0)
                            <div class="col-md-4 order-1 order-md-2" data-aos="fade-left">
                                <img src="/storage/<?= $detail['image'] ?>" class="img-fluid" alt="">
                            </div>
                        @endif
                        <div class="col-md-8 pt-4" data-aos="fade-up">
                            <h3><?= $detail['title'][app()->getLocale()] ?? '' ?></h3>
                            <p>
                                <?= $detail['description'][app()->getLocale()] ?? '' ?>
                            </p>
                        </div>
                    </div>
                @endforeach


            </div>
        </section><!-- End Details Section -->

        <!-- ======= Gallery Section ======= -->
        </section><!-- End Details Section -->

        <!-- ======= Gallery Section ======= -->
        <section id="gallery" class="gallery">
            <div class="container">

                <div class="section-title" data-aos="fade-up">
                    <h2>{{ __('all.portfoliolar') }} </h2>
                    <p>{{ __('all.ourportfolio') }}</p>
                </div>
                <div class="row g-0 aos-init aos-animate" data-aos="fade-left">
                    @foreach ($portfolios as $portfolio)
                        <div class="col-lg-3 col-md-4">
                            <div class="gallery-item aos-init aos-animate" data-aos="zoom-in" data-aos-delay="100">
                                <a href="{{ $portfolio->url }}" class="gallery-lightbox">
                                    <div class="img-block-gallery">
                                        <img src="/storage/{{ $portfolio->image }}" alt=""
                                            class="img-fluid"><br><br>
                                    </div>
                                    <div class="info-popup" id="infoPopup"> <a href="{{ $portfolio->url }}"></a></div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>



            </div>
        </section><!-- End Gallery Section -->

        <!-- ======= Testimonials Section ======= -->
        <section id="testimonials" class="testimonials">
            <div class="container">

                <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
                    <div class="swiper-wrapper">
                        @foreach ($corusel as $item)
                            <div class="swiper-slide">

                                <div class="testimonial-item">

                                    <div class="img-testimonials">
                                        <div class="img-blocks">

                                            <img src="/storage/<?= $item['image'] ?>" class="testimonial-img"
                                                alt="Testimonials" />
                                        </div>
                                    </div>
                                    <div class="paragraph">
                                        <h3><?= $item['name'][app()->getLocale()] ?? '' ?></h3>
                                        <h4><?= $item['profession'][app()->getLocale()] ?? '' ?></h4>
                                        <p>
                                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                            <?= $item['description'][app()->getLocale()] ?? '' ?>
                                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                        </p>
                                    </div>

                                </div>

                            </div><!-- End testimonial item -->
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                </div>

            </div>
        </section><!-- End Testimonials Section -->


        <section id="faq" class="faq section-bg">
            <div class="container">

                <div class="section-title" data-aos="fade-up">
                    <h2>{{ getTranslate('faq')->title[app()->getLocale()] ?? '' }}</h2>
                    <p>{{ getTranslate('faq')->content[app()->getLocale()] ?? '' }}</p>
                </div>

                <div class="faq-list">
                    <ul>
                        @foreach ($questions_answers as $qa)
                            <li data-aos="fade-up">
                                <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" class="collapse"
                                    data-bs-target="#faq-list-{{ $qa->id }}">{{ $qa->question[$app->getLocale()] ?? '' }}
                                    <i class="bx bx-chevron-down icon-show"></i><i
                                        class="bx bx-chevron-up icon-close"></i></a>
                                <div id="faq-list-{{ $qa->id }}" class="collapse" data-bs-parent=".faq-list">
                                    <p>
                                        {{ $qa->answer[$app->getLocale()] ?? '' }}
                                    </p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </section><!-- End F.A.Q Section -->

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact">
            <div class="container">

                <div class="section-title" data-aos="fade-up">
                    <h2>{{ getTranslate('contactus')->title[app()->getLocale()] ?? '' }}</h2>
                    <p>{{ getTranslate('contactus')->content[app()->getLocale()] ?? '' }}</p>
                </div>
                <div class="row">

                    <div class="col-lg-4" data-aos="fade-right" data-aos-delay="100">
                        <div class="info">
                            <div class="address">
                                <i class="bi bi-geo-alt"></i>
                                <h4>{{ getTranslate('location')->title[app()->getLocale()] ?? '' }}:</h4>
                                <p>{{ getTranslate('location')->content[app()->getLocale()] ?? '' }}</p>
                            </div>

                            <div class="email">
                                <i class="bi bi-envelope"></i>
                                <h4>{{ getTranslate('email')->title[app()->getLocale()] ?? '' }}:</h4>
                                <p>{{ getTranslate('email')->content[app()->getLocale()] ?? '' }}</p>
                            </div>

                            <div class="phone">
                                <i class="bi bi-phone"></i>
                                <h4>{{ getTranslate('call')->title[app()->getLocale()] ?? '' }}:</h4>
                                <p>{{ getTranslate('call')->content[app()->getLocale()] ?? '' }}</p>
                            </div>

                        </div>

                    </div>

                    <div class="col-lg-8 mt-5 mt-lg-0" data-aos="fade-left" data-aos-delay="200">

                        <div class="alert alert-success d-none success-message"></div>

                        <div class="error-message alert alert-danger d-none">
                        </div>
                        <form role="form" class="php-email-form">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="text" name="name" class="form-control" id="name"
                                        placeholder="{{ __('lang.your_name') }}" required>
                                    <small class="text-danger" id="err-name"></small>

                                </div>
                                <div class="col-md-6 form-group mt-3 mt-md-0">
                                    <input type="text" class="form-control " name="email" id="email"
                                        placeholder="{{ __('lang.your_email') }}">
                                    <small class="text-danger" id="err-email"></small>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <input type="text" class="form-control" name="number" id="number"
                                    placeholder="{{ __('lang.tel') }}" required>
                                <small class="text-danger" id="err-tel"></small>
                            </div>
                            <div class="form-group mt-3">
                                <textarea class="form-control" id="message" name="message" rows="5" placeholder="{{ __('lang.message') }}"
                                    required></textarea>
                                <small class="text-danger" id="err-message"></small>
                            </div>

                            <div class="text-center">
                                <button type="button" onclick="sendMessage()">{{ __('lang.send') }}</button>
                            </div>
                        </form>

                    </div>

                </div>

            </div>
        </section><!-- End Contact Section -->

    </main><!-- End #main -->
@endsection
@section('scripts')
    <script>
        function sendMessage() {
            //forms element
            const name = document.getElementById('name')
            const email = document.getElementById('email')
            const number = document.getElementById('number')
            const message = document.getElementById('message')

            // Errors and success
            const success = document.querySelector('.success-message');
            const err_name = document.getElementById("err-name");
            const err_email = document.getElementById("err-email");
            const err_tel = document.getElementById("err-tel");
            const err_message = document.getElementById("err-message");
            const url = "/contact";
            $.ajax({
                method: "POST",
                url: url,
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    name: name.value.trim(),
                    email: email.value.trim(),
                    number: number.value.trim(),
                    message: message.value.trim(),
                },
                success: function(data) {
                    success.innerHTML = data.message;
                    success.className = "alert alert-success success-message"
                    err_name.innerHTML = "";
                    err_email.innerHTML = "";
                    err_tel.innerHTML = "";
                    err_message.innerHTML = "";
                    name.value = "";
                    email.value = "";
                    number.value = "";
                    message.value = "";
                    name.classList.remove('is-invalid')
                    email.classList.remove('is-invalid')
                    number.classList.remove('is-invalid')
                    message.classList.remove('is-invalid')
                },
                error: function({
                    status,
                    responseJSON
                }) {
                    if (status === 422) {
                        success.innerHTML = "";
                        success.className = "alert alert-success success-message d-none"
                        console.log(responseJSON)
                        if (responseJSON.errors.name) {
                            err_name.innerHTML = responseJSON.errors.name[0]
                            name.classList.add('is-invalid')
                        } else {
                            err_name.innerHTML = "";
                            name.classList.remove('is-invalid')
                        }

                        if (responseJSON.errors.email) {
                            err_email.innerHTML = responseJSON.errors.email[0]
                            email.classList.add('is-invalid')
                        } else {
                            err_email.innerHTML = "";
                            email.classList.remove('is-invalid')
                        }

                        if (responseJSON.errors.number) {
                            err_tel.innerHTML = responseJSON.errors.number[0]
                            number.classList.add('is-invalid')
                        } else {
                            err_tel.innerHTML = "";
                            number.classList.remove('is-invalid')
                        }
                        if (responseJSON.errors.message) {
                            err_message.innerHTML = responseJSON.errors.message[0]
                            message.classList.add('is-invalid')
                        } else {
                            err_message.innerHTML = "";
                            message.classList.remove('is-invalid')
                        }
                    }


                    return;
                }
            })
        }
    </script>
@endsection
