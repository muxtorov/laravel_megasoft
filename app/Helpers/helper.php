<?php

use App\Models\Translate;
use Illuminate\Support\Facades\Cache;

if(!function_exists('getTranslate')){
    function getTranslate(string $key){
        $translate = Cache::rememberForever('translate-'.$key,function() use($key){
            return Translate::query()->where('key',$key)->first();
        });

        if(!$translate){
            $translate = new Translate();
        }

        return $translate;
    }
}

if(!function_exists('getLocales')){
    function getLocales(){
        return ['ru','uz','en'];
    }
}
