<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TranslateResource\Pages;
use App\Filament\Resources\TranslateResource\RelationManagers;
use App\Models\Translate;
use Filament\Forms;
use Filament\Forms\Components\FileUpload;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
class TranslateResource extends Resource
{
    protected static ?string $model = Translate::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('title.uz')->label(__('all.question').' UZ')->required(),
                Textarea::make('content.uz')->label(__('all.answer'). ' Uz')->required(),
                TextInput::make('title.ru')->label(__('all.question') . ' Ru')->required(),
                Textarea::make('content.ru')->label(__('all.answer') . ' Ru')->required(),
                TextInput::make('title.en')->label(__('all.question') . ' En')->required(),
                Textarea::make('content.en')->label(__('all.answer') . ' En')->required(),
                TextInput::make('key')->label('key')->required()->unique(),
                FileUpload::make('icon')->image()->nullable()
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('key'),
                TextColumn::make('title'),
                TextColumn::make('content'),
                ImageColumn::make('icon')
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTranslates::route('/'),
            'create' => Pages\CreateTranslate::route('/create'),
            'edit' => Pages\EditTranslate::route('/{record}/edit'),
        ];
    }    
}
