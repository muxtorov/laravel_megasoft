<?php

namespace App\Filament\Resources\CoruselResource\Pages;

use App\Filament\Resources\CoruselResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListCorusels extends ListRecords
{
    protected static string $resource = CoruselResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
