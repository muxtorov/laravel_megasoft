<?php

namespace App\Filament\Resources\CoruselResource\Pages;

use App\Filament\Resources\CoruselResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditCorusel extends EditRecord
{
    protected static string $resource = CoruselResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
