<?php

namespace App\Filament\Resources\CoruselResource\Pages;

use App\Filament\Resources\CoruselResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCorusel extends CreateRecord
{
    protected static string $resource = CoruselResource::class;
}
