<?php

namespace App\Filament\Resources;

use App\Filament\Resources\AddPortfolioResource\Pages;
use App\Filament\Resources\AddPortfolioResource\RelationManagers;
use App\Models\AddPortfolio;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\FileUpload;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ImageColumn;


class AddPortfolioResource extends Resource
{
    protected static ?string $model = AddPortfolio::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([

                TextInput::make('title.uz')->label(__('all.title').' UZ')->required(),
                TextInput::make('url'),
                FileUpload::make('image')->image()
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('title'),
                TextColumn::make('url'),
                ImageColumn::make('image')
                
//                    ->thumbnail('square')
                    ->url(fn (AddPortfolio $detail) => $detail->image_url)
                    ->label('Image')
                    ->sortable()
                    ->searchable()
                    ->alignLeft()
                    ->width('100px'),

            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAddPortfolios::route('/'),
            'create' => Pages\CreateAddPortfolio::route('/create'),
            'edit' => Pages\EditAddPortfolio::route('/{record}/edit'),
        ];
    }    
}
