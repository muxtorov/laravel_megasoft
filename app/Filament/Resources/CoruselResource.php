<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CoruselResource\Pages;
use App\Filament\Resources\CoruselResource\RelationManagers;
use App\Models\Corusel;
use Filament\Forms;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;

class CoruselResource extends Resource
{
    protected static ?string $model = Corusel::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('name.uz')->label(__('all.name').' UZ')->required(),
                Textarea::make('description.uz')->label(__('all.description').' UZ')->required(),
                TextInput::make('name.ru')->label(__('all.name').' UZ')->required(),
                Textarea::make('description.ru')->label(__('all.description').' RU')->required(),
                TextInput::make('name.en')->label(__('all.name').' UZ')->required(),
                Textarea::make('description.en')->label(__('all.description').' EN')->required(),
                TextInput::make('profession.uz')->label(__('all.profession').' UZ')->required(),
                TextInput::make('profession.ru')->label(__('all.profession').' RU')->required(),
                TextInput::make('profession.en')->label(__('all.profession').' EN')->required(),
                FileUpload::make('image')->image()->nullable()
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name'),
                TextColumn::make('profession'),
                TextColumn::make('description'),
                ImageColumn::make('image')
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCorusels::route('/'),
            'create' => Pages\CreateCorusel::route('/create'),
            'edit' => Pages\EditCorusel::route('/{record}/edit'),
        ];
    }
}
