<?php

namespace App\Filament\Resources\TranslateResource\Pages;

use App\Filament\Resources\TranslateResource;
use Filament\Pages\Actions;
use Filament\Pages\Actions\EditAction;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Support\Facades\Cache;

class EditTranslate extends EditRecord
{
    protected static string $resource = TranslateResource::class;


    protected function afterSave(): void
    {
        $key = $this->all()['data']['key'];
       Cache::forget('translate-'.$key);
    }

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make()
        ];
    }
}
