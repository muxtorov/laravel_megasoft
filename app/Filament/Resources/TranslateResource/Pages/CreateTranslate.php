<?php

namespace App\Filament\Resources\TranslateResource\Pages;

use App\Filament\Resources\TranslateResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTranslate extends CreateRecord
{
    protected static string $resource = TranslateResource::class;
}
