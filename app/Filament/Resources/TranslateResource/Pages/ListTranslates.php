<?php

namespace App\Filament\Resources\TranslateResource\Pages;

use App\Filament\Resources\TranslateResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListTranslates extends ListRecords
{
    protected static string $resource = TranslateResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
