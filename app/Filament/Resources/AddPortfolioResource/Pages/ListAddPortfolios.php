<?php

namespace App\Filament\Resources\AddPortfolioResource\Pages;

use App\Filament\Resources\AddPortfolioResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListAddPortfolios extends ListRecords
{
    protected static string $resource = AddPortfolioResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
