<?php

namespace App\Http\Controllers;

use App\Models\AddPortfolio;
use App\Models\Corusel;
use App\Models\Detail;
use App\Models\QuestionAnswer;

class HomeController extends Controller
{
    public function index()
    {
        $details = Detail::all();
        $portfolios = AddPortfolio::all();
        $questions_answers = QuestionAnswer::all();
        $corusel = Corusel::all();

        return view('home', compact('details', 'portfolios', 'questions_answers', 'corusel'));
    }


    public function lang(string $lang)
    {
        if (in_array($lang, getLocales())) {
            session()->put('lang', $lang);
        }

        return redirect()->back();
    }
}
