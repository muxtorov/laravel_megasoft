<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller
{
    /**
     * @param ContactRequest $request
     * @return RedirectResponse
     */
    public function sendMessage(ContactRequest $request)
    {
        Contact::query()->create($request->validated());
        
        return response()->json([
            'message' =>  __('all.your_message_sended_succussfull'),
        ]);
    }
}
