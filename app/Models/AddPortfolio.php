<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\FileUpload;



class AddPortfolio extends Model
{
    use HasFactory;
    protected $table = "add_portfolios";

    protected $casts = [
        'title' => 'json',
    ];

    protected $fillable = [
        'title',
        'url',
        'image'
    ];
}
