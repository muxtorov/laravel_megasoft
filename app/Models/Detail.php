<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    use HasFactory;
    protected $casts = [
        'title' => 'json',
        'description' => 'json'
    ];
    protected $fillable = [
        'title',
        'description',
        'image',
    ];
}
