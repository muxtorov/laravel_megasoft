<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    use HasFactory;

    protected $casts = [
        'question' => 'json',
        'answer' => 'json',

    ];

    protected $fillable = [
        'question',
        'answer',
    ];
}
