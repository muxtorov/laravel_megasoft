<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Corusel extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'profession',
        'description',
        'image'
    ];

    protected $casts = [
        'name' => 'json',
        'profession' => 'json',
        'description' => 'json',
    ];
}
