<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Translate extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'key',
        'content',
        'icon'
    ];

    protected $casts = [
        'title' => 'json',
        'content' => 'json'
    ];
}
